class ApplicationMailer < ActionMailer::Base
  default from: 'sico@example.com'
  layout 'mailer'
end
